package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 描述：
 *
 * @author gesiyao
 * @date 2021-06-16 14:32
 */

@SpringBootApplication
public class JenkinsGitlabTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(JenkinsGitlabTestApplication.class);
    }
}
