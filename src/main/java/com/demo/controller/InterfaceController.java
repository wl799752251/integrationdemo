package com.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * 描述：
 *
 * @author gesiyao
 * @date 2021-07-07 09:35
 */
@Controller
public class InterfaceController {
    @RequestMapping("/interface")
    public String interfaceJson(){
        return "/interface.json";
    }

    @RequestMapping("/index")
    public String index(){
        return "/index.html";
    }
}
